Adds Firebug Lite to any Drupal site.

To use this module:

1) You need to be a developer (why else would you want to debug JavaScript?)

- Install the module (admin -> build -> modules)
- Configure module permissions (admin -> user -> access)

Optional
- Settings page to turn off Firebug icon (admin -> settings -> firebug)

Key Binds:
open firebug: Ctrl (apple) + shift + L 

Links & More Info:
http://www.getfirebug.com/

Enjoy!

