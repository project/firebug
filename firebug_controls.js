// $Id:
// Firebug Controls

var firebug = {};

firebug.toggle = function(event) {
  if (!this.opened) {
    window.console.open();
    this.opened = true;
  } else {
    window.console.close();
    this.opened = false;
  }

  return false;
};

// Global Killswitch.
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    if (Drupal.settings.firebug.icon && !console.firebug) {
      var link = $('<a href="#firebug" title="Firebug"><img src="'+ Drupal.settings.firebug.path +'/firebug/firebug3.jpg" alt="Firebug" /><a>').click(firebug.toggle);
      $('<div id="firebug_icon"></div>').css({position: 'fixed', top: '5px', right: '10px'}).append(link).prependTo(document.body);
    }
  });
}
